
const express = require('express');


const TaskController = require('../controllers/TaskController.js')

const router = express.Router()

router.get('/', (req, res) => {
	TaskController.getAllTasks().then((resultFromController) => res.send(resultFromController))
})

router.post('/create', (req, res) => {
	TaskController.createTask(req.body).then((resultFromController) => res.send(resultFromController))
})

router.put('/:id/update', (req, res) => {
	TaskController.updateTask(req.params.id, req.body)
})


router.delete('/:id/delete', (req, res) => {
	TaskController.deleteTask(req.params.id).then((resultFromController) => res.send(resultFromController))
})

router.get('/:id/get', (req, res) => {
	TaskController.getTask(req.params.id).then((resultFromController) => res.send(resultFromController))
})

router.get('/:id/complete', (req, res) => {
	TaskController.completeTask(req.params.id).then((resultFromController) => res.send(resultFromController))
})



module.exports = router