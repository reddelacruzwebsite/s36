// Controllers

const Task = require('../models/Task.js');


module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
};


module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error) {
			console.log(error)
			return false
		} else if(savedTask != null  && savedTask.name == reqBody.name) {
			return 'Duplicate Task Found!'
		} else {
			return savedTask
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			result.name = newContent.name
			return result.save().then((updatedTask, error) => {
				if(error) {
					console.log(error)
					return false
				} else {
					return updatedTask
				}
			})
		}
	})
}


module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((deletedTask, error) => {
		if(error) {
			console.log(error); 
			return false
		} else {
			return deletedTask
		}
	})
}

module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((takenTask, error) => {
		if(error) {
			console.log(error); 
			return false
		} else {
			return takenTask
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			result.status = newContent.status
			return result.save().then((updatedTask, error) => {
				if(error) {
					console.log(error)
					return false
				} else {
					return updatedTask
				}
			})
		}
	})
}






