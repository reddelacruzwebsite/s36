// Express server setup
const express = require('express');
const mongoose = require('mongoose');

const taskRoutes = require('./routes/taskRoutes.js')
const app = express();

const port = 3001;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect(`mongodb+srv://reddelacruzwebsite:admin123@cluster0.itpeetc.mongodb.net/s36?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', () => console.error('Connection Error'));
db.once('open', () => console.log('Connected to MongoDB!'))


app.use('/tasks', taskRoutes)
app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.listen(port, () => console.log(`Server is running at port ${port}`))